;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Harold carter"
      user-mail-address "hcarter1112@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;

;; (setq doom-font "JetBrainsMono NF-12")

(setq doom-font (font-spec :family "JetBrainsMono NF" :size 16 :weight 'medium)
    doom-variable-pitch-font (font-spec :family "JetBrainsMono NF" :size 17)
    doom-unicode-font "all-the-icons-13")
(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
  :slant 'italic)
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-spacegrey)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")



;; (use-package! elpy
;;   init:
;;   (elpy-enable))

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
(load! "C:/Users/dirtb/.doom.d/powershell.el/powershell.el")
(add-load-path! "C:/Users/dirtb/.doom.d/powershell.el")
(use-package! powershell
  :demand t
  :config
  (add-to-list 'auto-mode-alist '("\\.ps1\\'" . powershell-mode)))

(fset 'evil-up-center
   (kmacro-lambda-form [?\C-u ?z ?z] 0 "%d"))
(fset 'evil-down-center
   (kmacro-lambda-form [?\C-d ?z ?z] 0 "%d"))

(map! :desc "Quick-tab right" :nr "L" #'switch-to-next-buffer)
(map! :desc "Quick-tab left" :nr "H" #'switch-to-prev-buffer)

(map! :desc "UP half page" :nr "C-u" #'evil-scroll-up)
(map! :desc "DOWN half page" :nr "C-d" #'evil-scroll-down)

;; (map! :desc "UP half page, cursor centered" :n "C-u" #'evil-up-center)
;; (map! :desc "DOWN half page, cursor centered" :n "C-d" #'evil-down-center)

;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
