winget-progs

   - 
   \ 
   | 
   / 
   - 
                                                                                                                        

   - 
                                                                                                                        
Name                                    Id                                      Version            Available     Source
-----------------------------------------------------------------------------------------------------------------------
Bitwarden                               Bitwarden.Bitwarden                     2023.5.1           2023.7.0      winget
NVM for Windows 1.1.11                  CoreyButler.NVMforWindows               1.1.11                           winget
VIA 2.0.5                               Olivia.VIA                              2.0.5              3.0.0.0       winget
WhatsApp                                5319275A.WhatsAppDesktop_cv1g1gvanyjgm  2.2327.6.0                       
7-Zip 23.01 (x64)                       7zip.7zip                               23.01                            winget
Visual Studio Build Tools 2022 (2)      Microsoft.VisualStudio.2022.BuildTools  17.6.4             17.6.5        winget
Pomotroid 0.13.0                        splode.pomotroid                        0.13.0                           winget
Visual Studio Professional 2022         Microsoft.VisualStudio.2022.ProfessionΓÇª 17.6.4             17.6.5        winget
AMD Software                            AMD Catalyst Install Manager            23.7.1                           
AMD Ryzen Master                        AMD Ryzen Master                        2.10.2.2367                      
AMD Chipset Software                    AMD_Chipset_IODrivers                   4.07.13.2243                     
Advanced Combat Tracker (remove only)   EQAditu.AdvancedCombatTracker           3.6.2.279                        winget
AMD Radeon Software                     AdvancedMicroDevicesInc-RSXCM_fhmx3h6dΓÇª 22.10.0.0                        
AutoHotkey 1.1.37.01                    AutoHotkey.AutoHotkey                   1.1.37.01          2.0.4         winget
ARMOURY CRATE                           B9ECED6F.ArmouryCrate_qmba6cd70vzyy     5.6.8.0                          
Battle.net                              Battle.net                              Unknown                          
Brave                                   Brave.Brave                             114.1.52.130                     winget
CPUID HWMonitor 1.50                    CPUID.HWMonitor                         1.50                             winget
Ubuntu                                  Canonical.Ubuntu.2204                   2204.2.33.0                      winget
EPSON L395 Series Printer Uninstall     EPSON L395 Series                       Unknown                          
Epson Scan 2                            Epson Scan 2                            Unknown                          
Everything 1.4.1.1024 (x64)             voidtools.Everything                    1.4.1.1024                       winget
Foxit PDF Reader                        Foxit.FoxitReader                       12.1.2.15332                     winget
Git                                     Git.Git                                 2.41.0             2.41.0.2      winget
Microsoft Edge                          Microsoft.Edge                          114.0.1823.82      114.0.1823.86 winget
Microsoft Edge Update                   Microsoft Edge Update                   1.3.177.11                       
Microsoft Edge WebView2 Runtime         Microsoft.EdgeWebView2Runtime           114.0.1823.82                    winget
Minecraft Launcher                      Microsoft.4297127D64EC6_8wekyb3d8bbwe   1.2.4.0                          
Cortana                                 Microsoft.549981C3F5F10_8wekyb3d8bbwe   4.2204.13303.0                   
App Installer                           Microsoft.DesktopAppInstaller_8wekyb3dΓÇª 1.20.1572.0                      
Xbox                                    Microsoft.GamingApp_8wekyb3d8bbwe       2306.1001.16.0                   
Gaming Services                         Microsoft.GamingServices_8wekyb3d8bbwe  13.78.12002.0                    
Get Help                                Microsoft.GetHelp_8wekyb3d8bbwe         10.2303.10961.0                  
HEIF Image Extensions                   Microsoft.HEIFImageExtension_8wekyb3d8ΓÇª 1.0.61171.0                      
HEVC Video Extensions from Device ManuΓÇª Microsoft.HEVCVideoExtension_8wekyb3d8ΓÇª 2.0.61591.0                      
Microsoft Edge                          Microsoft.MicrosoftEdge.Stable_8wekyb3ΓÇª 114.0.1823.82                    
Outlook for Windows                     Microsoft.OutlookForWindows_8wekyb3d8bΓÇª 1.2023.630.100                   
Paint                                   Microsoft.Paint_8wekyb3d8bbwe           11.2302.19.0                     
Microsoft People                        Microsoft.People_8wekyb3d8bbwe          10.2202.31.0                     
Power Automate                          Microsoft.PowerAutomateDesktop_8wekyb3ΓÇª 1.0.529.0                        
PowerToys ImageResizer Context Menu     Microsoft.PowerToys.ImageResizerContexΓÇª 0.71.0.0                         
PowerToys PowerRename Context Menu      Microsoft.PowerToys.PowerRenameContextΓÇª 0.71.0.0                         
Raw Image Extension                     Microsoft.RawImageExtension_8wekyb3d8bΓÇª 2.1.61661.0                      
Snipping Tool                           Microsoft.ScreenSketch_8wekyb3d8bbwe    11.2303.17.0                     
Windows Security                        Microsoft.SecHealthUI_8wekyb3d8bbwe     1000.25873.9001.0                
Store Experience Host                   Microsoft.StorePurchaseApp_8wekyb3d8bbΓÇª 22305.1401.2.0                   
VP9 Video Extensions                    Microsoft.VP9VideoExtensions_8wekyb3d8ΓÇª 1.0.61591.0                      
Web Media Extensions                    Microsoft.WebMediaExtensions_8wekyb3d8ΓÇª 1.0.61591.0                      
Webp Image Extensions                   Microsoft.WebpImageExtension_8wekyb3d8ΓÇª 1.0.52351.0                      
Microsoft Photos                        Microsoft.Windows.Photos_8wekyb3d8bbwe  2023.11050.16005.0               
Windows Clock                           Microsoft.WindowsAlarms_8wekyb3d8bbwe   11.2304.0.0                      
Windows Calculator                      Microsoft.WindowsCalculator_8wekyb3d8bΓÇª 11.2210.0.0                      
Windows Camera                          Microsoft.WindowsCamera_8wekyb3d8bbwe   2023.2305.4.0                    
Windows Maps                            Microsoft.WindowsMaps_8wekyb3d8bbwe     11.2303.5.0                      
Windows Notepad                         Microsoft.WindowsNotepad_8wekyb3d8bbwe  11.2305.18.0                     
Windows Sound Recorder                  Microsoft.WindowsSoundRecorder_8wekyb3ΓÇª 11.2304.25.0                     
Microsoft Store                         Microsoft.WindowsStore_8wekyb3d8bbwe    22306.1401.1.0                   
Windows Terminal                        Microsoft.WindowsTerminal               1.17.11461.0                     winget
Windows Package Manager Source (winget) Microsoft.Winget.Source_8wekyb3d8bbwe   2023.718.2024.804                
Xbox TCUI                               Microsoft.Xbox.TCUI_8wekyb3d8bbwe       1.24.10001.0                     
Xbox Console Companion                  Microsoft.XboxApp_8wekyb3d8bbwe         48.89.25001.0                    
Xbox Game Bar Plugin                    Microsoft.XboxGameOverlay_8wekyb3d8bbwe 1.54.4001.0                      
Xbox Game Bar                           Microsoft.XboxGamingOverlay_8wekyb3d8bΓÇª 5.823.3261.0                     
Xbox Identity Provider                  Microsoft.XboxIdentityProvider_8wekyb3ΓÇª 12.95.3001.0                     
Xbox Game Speech Window                 Microsoft.XboxSpeechToTextOverlay_8wekΓÇª 1.21.13002.0                     
Phone Link                              Microsoft.YourPhone_8wekyb3d8bbwe       1.23052.121.0                    
Quick Assist                            MicrosoftCorporationII.QuickAssist_8weΓÇª 2.0.21.0                         
Windows Web Experience Pack             MicrosoftWindows.Client.WebExperience_ΓÇª 423.13900.0.0                    
Mozilla Firefox (x64 en-US)             Mozilla.Firefox                         115.0.2                          winget
Mozilla Maintenance Service             MozillaMaintenanceService               115.0.1                          
Notepad++ (64-bit x64)                  Notepad++.Notepad++                     8.5.4                            winget
Notepad++                               NotepadPlusPlus_7njy0v32s6xk6           1.0.0.0                          
OBS Studio                              OBSProject.OBSStudio                    29.1.3                           winget
Oh My Posh version 17.11.2 (All users)  JanDeDobbeleer.OhMyPosh                 17.11.2                          winget
Overwatch                               Overwatch                               Unknown                          
Python 3.10                             PythonSoftwareFoundation.Python.3.10_qΓÇª 3.10.3056.0                      
Razer Synapse                           Razer Synapse                           3.8.0630.062814                  
Realtek Audio Control                   RealtekSemiconductorCorp.RealtekAudioCΓÇª 1.27.254.0                       
Rustup: the Rust toolchain installer    Rustlang.Rustup                         1.26.0                           winget
Steam                                   Valve.Steam                             2.10.91.91                       winget
Poly Bridge 2                           Steam App 1062160                       Unknown                          
New World                               Steam App 1063730                       Unknown                          
Horizon Zero Dawn                       Steam App 1151640                       Unknown                          
STAR WARS Jedi: Fallen OrderTM          Steam App 1172380                       Unknown                          
No Man's Sky                            Steam App 275850                        Unknown                          
Poly Bridge                             Steam App 367450                        Unknown                          
Paladins                                Steam App 444090                        Unknown                          
Valheim                                 Steam App 892970                        Unknown                          
VBCABLE, The Virtual Audio Cable        VB:VBCABLE {87459874-1236-4469}         Unknown                          
VLC media player                        VideoLAN.VLC                            3.0.18                           winget
VooV Meeting                            Tencent.VooVMeeting                     3.16.4.510                       winget
JW Library                              WatchtowerBibleandTractSo.45909CDBADF3ΓÇª 14.0.138.0                       
XIVLauncher                             goatcorp.XIVLauncher                    6.3.6                            winget
Ferdium 6.4.0                           Ferdium.Ferdium                         6.4.0                            winget
Obsidian                                Obsidian.Obsidian                       1.3.5                            winget
Mail and Calendar                       microsoft.windowscommunicationsapps_8wΓÇª 16005.14326.21502ΓÇª               
GameSDK Service                         {021d69c3-d686-4a94-8fb5-fd1ee782fb14}  1.0.5.0                          
Microsoft Visual C++ 2013 RedistributaΓÇª Microsoft.VCRedist.2013.x64             12.0.40664.0                     winget
Intel┬« Driver & Support Assistant       Intel.IntelDriverAndSupportAssistant    23.3.25.6                        winget
Windows 11 Installation Assistant       Microsoft.WindowsInstallationAssistant  1.4.19041.2063                   winget
Node.js                                 OpenJS.NodeJS                           20.4.0                           winget
Windows SDK AddOn                       {15941C7F-810D-41DF-8C5A-8D0490277AFB}  10.1.0.0                         
Microsoft Visual C++ 2010  x64 RedistrΓÇª Microsoft.VCRedist.2010.x64             10.0.40219                       winget
Microsoft GameInput                     {1F2B6AF3-C260-8666-5950-E3FEDBC851D6}  10.1.22621.3036                  
Python Launcher                         {23514291-DEF3-42FD-A67C-A96E35C92F24}  3.11.4150.0                      
Microsoft Windows Desktop Runtime - 7.ΓÇª Microsoft.DotNet.DesktopRuntime.7       7.0.9                            winget
Epson Software Updater                  {2A369D40-CE23-421A-8173-3C303A0A8355}  4.6.6                            
ROG Live Service                        {2D87BFB6-C184-4A59-9BBE-3E20CE797631}  2.1.4.0                          
ASUS Framework Service                  {339A6383-7862-46DA-8A9D-E84180EF9424}  4.0.0.9                          
Microsoft Visual C++ 2012 RedistributaΓÇª Microsoft.VCRedist.2012.x86             11.0.61030.0                     winget
AniMe Matrix MB EN                      {399B6DA7-B609-426E-95F8-B9A83FB7D06E}  1.0.1                            
Windows Subsystem for Linux WSLg PreviΓÇª {3CBDE512-7510-4F90-B1C0-7C4EB9DD7C26}  1.0.27                           
PowerShell 7-x64                        Microsoft.PowerShell                    7.3.6.0                          winget
Python 3.11.4 (64-bit)                  {3d45edf4-44bb-483f-9e08-43c38c81e118}  3.11.4150.0                      
Microsoft Visual C++ 2015-2022 RedistrΓÇª {410c0ee1-00bb-41b6-9772-e12c2828b02f}  14.36.32532.0                    
Microsoft Update Health Tools           {43D501A5-E5E3-46EC-8F33-9E15D2A2CBD5}  5.70.0.0                         
New World Scheduler version 7.0         {480D8AEB-9788-4F3A-BA42-B8E0F47448BA}ΓÇª 7.0                              
EA app                                  ElectronicArts.EADesktop                12.200.0.5457      12.231.0.5488 winget
JWLMerge version 2.1.0.4                {53082E90-DEA3-405D-B4C8-6495076D3D98}ΓÇª 2.1.0.4                          
Epson Printer Connection Checker        {562C1C83-6199-49DD-987B-60D5FF7BC971}  3.3.2.0                          
vs_CoreEditorFonts                      {56FB5923-1A95-4D55-BE78-CD42B50E67AD}  17.6.33605                       
ROGFontInstaller                        {605108C1-153E-43D8-8A67-7CE326B00ECA}  1.0.0                            
PowerShell 7.3.5.0-x64                  Microsoft.PowerShell                    7.3.5.0            7.3.6.0       winget
Microsoft Visual Studio Installer       {6F320B93-EE3C-4826-85E0-ADF79F8D4C61}  3.6.2115.31769                   
Windows PC Health Check                 Microsoft.WindowsPCHealthCheck          3.7.2204.15001                   winget
Realtek Ethernet Controller Driver      {8833FFB6-5B0C-4764-81AA-06DFEED9A476}  11.9.0614.2022                   
Microsoft Visual C++ 2015-2022 RedistrΓÇª {8bdfe669-9705-4184-9368-db9ce581e0e7}  14.36.32532.0                    
ASUS Motherboard                        {93795eb8-bd86-4d4d-ab27-ff80f9467b37}  4.01.03                          
EpsonNet Print                          {96ED1D58-440C-4345-8FEE-C4781366C67F}  3.1.4.0                          
Microsoft Visual C++ 2013 RedistributaΓÇª Microsoft.VCRedist.2013.x86             12.0.40664.0                     winget
Epson Customer Research Participation   {B26449A6-6007-4460-B4FE-C4776115BCEA}  1.83.0000                        
LibreOffice 7.5.4.2                     TheDocumentFoundation.LibreOffice       7.5.4.2                          winget
Universal Adb Driver                    ClockworkMod.UniversalADBDriver         1.0.4                            winget
STAR WARS Jedi - Fallen OrderTM         {D00A89F1-2D8C-4589-B1D1-73A6544E3B1F}  Unknown                          
Epson Event Manager                     {E244A764-EDD0-46B0-8689-661F6B28D9E5}  3.10.0069                        
Microsoft Visual Studio Code            Microsoft.VisualStudioCode              1.80.1                           winget
Zoom (64-bit)                           Zoom.Zoom                               > 5.15.3.18551                   winget
Flameshot                               Flameshot.Flameshot                     12.1.0                           winget
ARMOURY CRATE Lite Service              {EF3944FF-2501-4568-B15C-5701E726719E}  5.6.8                            
Microsoft Visual C++ 2010  x86 RedistrΓÇª Microsoft.VCRedist.2010.x86             10.0.40219                       winget
Realtek Audio Driver                    {F132AF7F-7BCA-4EDE-8A7C-958108FE7DBC}  6.0.9222.1                       
Windows Subsystem for Linux Update      {F8474A47-8B5D-4466-ACE3-78EAB3BF21A8}  5.10.102.1                       
PowerToys (Preview) x64                 Microsoft.PowerToys                     0.71.0                           winget
AURA Service                            {be345e17-83f7-4b5f-b533-6f975b9a8180}  3.07.17                          
Windows Software Development Kit - WinΓÇª Microsoft.WindowsSDK.10.0.22000         10.0.22000.832                   winget
Microsoft Windows Desktop Runtime - 7.ΓÇª Microsoft.DotNet.DesktopRuntime.7       7.0.9                            winget
